import ArticleCard from './ArticleCard';
import styles from '../styles/Article.module.css';

const Articles = ({ articles }) => {

	const render_articles = articles.map(article => (
    <ArticleCard key={article.id} article={article} />
  ));

  return (
    <div className={styles.grid}>
			{ render_articles }
    </div>
  );
};

export default Articles;
