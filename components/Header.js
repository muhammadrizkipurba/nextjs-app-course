import styles from '../styles/Header.module.css';

function Header() {
  // const x = 1;
  return (
    <div> 
      <h1 className={styles.title}>
        <span>Whello</span> News
      </h1>    
      <p className={styles.description}>
        Keep up to date with the latest online marketing news
      </p>
      {/* IN COMPONENT STYLES */}
      {/* <style jsx>
        {`
          .title {
            color: ${x > 3 ? 'red' : 'blue'}
          }
        `}
      </style> */}
    </div>
  );
};

export default Header;
