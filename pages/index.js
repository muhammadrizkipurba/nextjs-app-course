import Head from 'next/head';
import Articles from '../components/Articles';

const Home = ({ articles }) => {
  return (
    <div>
      <Head>
        <title>Whellonews</title>
        <meta name="keywords" content="web development, whello, news, whellonews" />
      </Head>
      <Articles articles={articles} />
    </div>
  );
};

export const getStaticProps = async() => {
  const res = await fetch(`https://jsonplaceholder.typicode.com/posts?_limit=6`);
  const articles = await res.json();
  const result = {
    props: {
      articles
    }
  };

  return result;
};

export default Home;