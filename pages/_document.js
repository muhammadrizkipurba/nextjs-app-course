import Document, { Html, Head, Main, NextScript } from 'next/document'

// to create custom html attribute 
class MyDocument extends Document {
  render() {
    return (
      <Html lang='en'>
        <Head />
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument