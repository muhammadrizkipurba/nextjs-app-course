import Link from 'next/link';
// import { useRouter } from 'next/router';

const article = ({ article }) => {
    // const router = useRouter();
    // const { title } = router.query;

    return (
        <>
            <h1>{article.title}</h1>
            <p>{article.body}</p>
            <br/>
            <Link href='/'>
                Back
            </Link>
        </>
    );
};

// To fetch every time the params id change, but if the params id is outside of articles data in props, it will be a 404 page response
// export const getServerSideProps = async(context) => {
//     const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${context.params.id}`);
//     const article = await res.json();
//     const result = {
//         props: {
//             article
//         }
//     };

//     return result;
// };


// this way is more faster, it get all of the posts and serve all pages static ( NB: Not recommended for larger website )
export const getStaticProps = async(context) => {
    const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${context.params.id}`);
    const article = await res.json();
    const result = {
        props: {
            article
        }
    };

    return result;
};

export const getStaticPaths = async() => {
    const res = await fetch(`https://jsonplaceholder.typicode.com/posts?_limit=10`);
    
    const articles = await res.json();
    const ids = articles.map(article => article.id);
    const paths = ids.map(id => ({ params: { id: id.toString() }}));

    const result = {
        // paths: { params: { id: '1', id: '2' }}
        paths,
        fallback: false
    };

    return result;
}

export default article;
