import Head from "next/head";

const about = () => {
  return (
    <div>
      <Head>
        <title>About us</title>
        <meta name="keywords" content="about us" />
      </Head>
      <h1>About page</h1>
    </div>
  );
};

export default about;
